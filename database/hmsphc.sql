-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jan 24, 2019 at 04:11 PM
-- Server version: 5.7.21
-- PHP Version: 5.6.35

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hmsphc`
--

-- --------------------------------------------------------

--
-- Table structure for table `add_bed`
--

DROP TABLE IF EXISTS `add_bed`;
CREATE TABLE IF NOT EXISTS `add_bed` (
  `bed_id` int(100) NOT NULL AUTO_INCREMENT,
  `bed_category` varchar(200) DEFAULT NULL,
  `bed_num` varchar(200) DEFAULT NULL,
  `deb_des` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`bed_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `add_bed`
--

INSERT INTO `add_bed` (`bed_id`, `bed_category`, `bed_num`, `deb_des`) VALUES
(1, 'special ward', '135', 'cjlkaks akjlkj');

-- --------------------------------------------------------

--
-- Table structure for table `add_bedcat`
--

DROP TABLE IF EXISTS `add_bedcat`;
CREATE TABLE IF NOT EXISTS `add_bedcat` (
  `cat_id` int(50) NOT NULL AUTO_INCREMENT,
  `category` varchar(200) DEFAULT NULL,
  `description` varchar(110) DEFAULT NULL,
  PRIMARY KEY (`cat_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `add_bedcat`
--

INSERT INTO `add_bedcat` (`cat_id`, `category`, `description`) VALUES
(1, 'Special Ward', '150 Beds'),
(2, 'General Ward', '2000 Beds'),
(3, 'ICU', '200 Beds');

-- --------------------------------------------------------

--
-- Table structure for table `ci_users`
--

DROP TABLE IF EXISTS `ci_users`;
CREATE TABLE IF NOT EXISTS `ci_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `firstname` varchar(30) NOT NULL,
  `lastname` varchar(30) NOT NULL,
  `email` varchar(50) NOT NULL,
  `mobile_no` varchar(30) NOT NULL,
  `password` varchar(255) NOT NULL,
  `is_admin` tinyint(4) NOT NULL DEFAULT '0',
  `last_ip` varchar(30) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ci_users`
--

INSERT INTO `ci_users` (`id`, `username`, `firstname`, `lastname`, `email`, `mobile_no`, `password`, `is_admin`, `last_ip`, `created_at`, `updated_at`) VALUES
(3, 'demo', 'admin', 'admin', 'admin@admin.com', '12345', '$2y$10$tFY/JX/rEKR8ODW2ktjYtOWf3zTkvOtynrXOvrcZ2Qm9h72r9TaPW', 1, '', '2017-09-29 10:09:44', '2017-09-30 08:09:29'),
(7, 'Test1', 'test', 'champion', 'test@gmail.com', '12345', '$2y$10$J317ib3JnglmhO.IbaADHOyr4j2xSbWZZtO8pHDWW65GUZLZEu63u', 0, '', '2017-09-29 11:09:02', '2017-09-30 08:09:51'),
(6, 'Ali Raza', 'Ali', 'Raza', 'ali@admin.com', '123456', '$2y$10$RoUcgnJ1AaK125c/hFmkWexGRvEhvQKXm21YRYlNrEHuvQcH2zMMG', 0, '', '2017-10-03 06:10:31', '2017-10-03 05:10:25'),
(5, 'wwe champion', 'wwe', 'champion', 'naumanahmedcs@gmail.com', '12345', '$2y$10$KB0NxzAOWtbnVj.7OJujRe7G5K1lb6UG5ra3PnAAt/Oc96Wfl5tea', 0, '', '2017-09-29 11:09:02', '2017-10-03 06:10:51'),
(8, 'John Smith', 'John', 'Smith', 'johnsmith@gmail.com', '12345', '$2y$10$J317ib3JnglmhO.IbaADHOyr4j2xSbWZZtO8pHDWW65GUZLZEu63u', 0, '', '2017-09-29 11:09:02', '2017-09-30 08:09:51'),
(9, 'Herry Jhone', 'Herry', 'Jhone', 'herrypro@gmail.com', '449548545624', '$2y$10$.P.vz6NaSbLPq.BvOY0umulTKBj9Ovds2jaQBdGbyKzlfjOV0O4RW', 0, '', '2017-10-03 07:10:26', '2017-10-03 07:10:26');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
