<?php
/**
 * Created by PhpStorm.
 * User: Yakairol-Charan
 * Date: 13-11-2018
 * Time: 16:38
 */

class Addbed_model extends CI_Model
{
    public function selectbed(){
        $this->db->select('*');
        $query=$this->db->get('add_bed');
        return $query->result();
    }

    function fetch_bed_detail($customer_id){
        $this->db->where("bed_id",$customer_id);
        $query=$this->db->get('add_bed');
        return $query->result();
    }

    function add_bed($data){
        $this->db->insert("add_bed", $data);
    }

    function update_bed($customer_id,$data){
        $this->db->where("bed_id",$customer_id);
        $this->db->update("add_bed",$data);
    }

    function delete_bed($customer_id)
    {
        $this->db->where("bed_id", $customer_id);
        $this->db->delete("add_bed");
        //DELETE FROM users WHERE id = '$user_id'
    }
}