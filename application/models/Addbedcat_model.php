<?php
/**
 * Created by PhpStorm.
 * User: Yakairol-Charan
 * Date: 13-11-2018
 * Time: 16:38
 */

class Addbedcat_model extends CI_Model
{
    public function selectvendor(){
        $this->db->select('*');
        $query=$this->db->get('add_bedcat');
        return $query->result();
    }

    function fetch_cat_detail($customer_id){
        $this->db->where("cat_id",$customer_id);
        $query=$this->db->get('add_bedcat');
        return $query->result();
    }

    function add_bedcat($data){
        $this->db->insert("add_bedcat", $data);
    }

    function update_bedcat($customer_id,$data){
        $this->db->where("cat_id",$customer_id);
        $this->db->update("add_bedcat",$data);
    }

    function delete_bedcat($customer_id)
    {
        $this->db->where("cat_id", $customer_id);
        $this->db->delete("add_bedcat");
        //DELETE FROM users WHERE id = '$user_id'
    }
}