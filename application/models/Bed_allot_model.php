<?php
/**
 * Created by PhpStorm.
 * User: Yakairol-Charan
 * Date: 13-11-2018
 * Time: 16:38
 */

class Bed_allot_model extends CI_Model
{
    public function selectTable(){
        $this->db->select('*');
        $query=$this->db->get('bed_allot');
        return $query->result();
    }

    function fetch_allot_detail($customer_id){
        $this->db->where("allot_id",$customer_id);
        $query=$this->db->get('bed_allot');
        return $query->result();
    }

    function add_allot($data){
        $this->db->insert("bed_allot", $data);
    }

    function update_allot($customer_id,$data){
        $this->db->where("allot_id",$customer_id);
        $this->db->update("bed_allot",$data);
    }

    function delete_allot($customer_id)
    {
        $this->db->where("allot_id", $customer_id);
        $this->db->delete("bed_allot");
        //DELETE FROM users WHERE id = '$user_id'
    }
}