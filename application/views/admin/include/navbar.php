

  <header class="main-header">
    <!-- Logo -->
    <a href="<?= base_url('admin/dashboard');?>" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"style="font-family:'Times New Roman' "><b>HMS</b></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg" style="font-family:'Times New Roman' "><b>PHC-HMS </b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
             <!-- Messages: style can be found in dropdown.less-->
          <li class="dropdown messages-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-envelope-o"></i>
<!--              <span class="label label-success">4</span>-->
            </a>
<!--            <ul class="dropdown-menu">-->
<!--              <li class="header">You have 4 messages</li>-->
<!--              <li>-->
<!--                <!-- inner menu: contains the actual data -->
<!--                <ul class="menu">-->
<!--                  <li><!-- start message -->
<!--                    <a href="#">-->
<!--                      <div class="pull-left">-->
<!--                        <img src="--><?//= base_url()?><!--/public/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">-->
<!--                      </div>-->
<!--                      <h4>-->
<!--                        Support Team-->
<!--                        <small><i class="fa fa-clock-o"></i> 5 mins</small>-->
<!--                      </h4>-->
<!--                      <p>Why not buy a new awesome theme?</p>-->
<!--                    </a>-->
<!--                  </li>-->
<!--                  <!-- end message -->
<!--                  <li>-->
<!--                    <a href="#">-->
<!--                      <div class="pull-left">-->
<!--                        <img src="--><?//= base_url()?><!--/public/dist/img/user3-128x128.jpg" class="img-circle" alt="User Image">-->
<!--                      </div>-->
<!--                      <h4>-->
<!--                        AdminLTE Design Team-->
<!--                        <small><i class="fa fa-clock-o"></i> 2 hours</small>-->
<!--                      </h4>-->
<!--                      <p>Why not buy a new awesome theme?</p>-->
<!--                    </a>-->
<!--                  </li>-->
<!--                  <li>-->
<!--                    <a href="#">-->
<!--                      <div class="pull-left">-->
<!--                        <img src="--><?//= base_url()?><!--/public/dist/img/user4-128x128.jpg" class="img-circle" alt="User Image">-->
<!--                      </div>-->
<!--                      <h4>-->
<!--                        Developers-->
<!--                        <small><i class="fa fa-clock-o"></i> Today</small>-->
<!--                      </h4>-->
<!--                      <p>Why not buy a new awesome theme?</p>-->
<!--                    </a>-->
<!--                  </li>-->
<!--                  <li>-->
<!--                    <a href="#">-->
<!--                      <div class="pull-left">-->
<!--                        <img src="--><?//= base_url()?><!--/public/dist/img/user3-128x128.jpg" class="img-circle" alt="User Image">-->
<!--                      </div>-->
<!--                      <h4>-->
<!--                        Sales Department-->
<!--                        <small><i class="fa fa-clock-o"></i> Yesterday</small>-->
<!--                      </h4>-->
<!--                      <p>Why not buy a new awesome theme?</p>-->
<!--                    </a>-->
<!--                  </li>-->
<!--                  <li>-->
<!--                    <a href="#">-->
<!--                      <div class="pull-left">-->
<!--                        <img src="--><?//= base_url()?><!--/public/dist/img/user4-128x128.jpg" class="img-circle" alt="User Image">-->
<!--                      </div>-->
<!--                      <h4>-->
<!--                        Reviewers-->
<!--                        <small><i class="fa fa-clock-o"></i> 2 days</small>-->
<!--                      </h4>-->
<!--                      <p>Why not buy a new awesome theme?</p>-->
<!--                    </a>-->
<!--                  </li>-->
<!--                </ul>-->
<!--              </li>-->
<!--              <li class="footer"><a href="#">See All Messages</a></li>-->
<!--            </ul>-->
          </li>
          <!-- Notifications: style can be found in dropdown.less -->
          <li class="dropdown notifications-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-bell-o"></i>
<!--              <span class="label label-warning">10</span>-->
            </a>
            <ul class="dropdown-menu">
              <li class="header">Notifications</li>
              <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu">
                  </li>
                </ul>
              </li>
              <li class="footer"><a href="#">View all</a></li>
            </ul>
          </li>
          <!-- Tasks: style can be found in dropdown.less -->

          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="<?= base_url() ?>public/dist/img/user2-160x160.jpg" class="user-image" alt="User Image">
              <span class="hidden-xs"><?= ucwords($this->session->userdata('name')); ?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="<?= base_url() ?>public/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">

                <p>
                    <?= ucwords($this->session->userdata('name')); ?>
                </p>
              </li>
              <!-- Menu Body -->
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-right">
                  <a href="<?= site_url('admin/auth/logout'); ?>" class="btn btn-default btn-flat">Sign out</a>
                </div>
                <div class="pull-left">
                  <a href="<?= site_url('admin/auth/change_pwd'); ?>" class="btn btn-default btn-flat">Change Password</a>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->

        </ul>
      </div>
    </nav>
  </header>
