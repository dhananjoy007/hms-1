<!DOCTYPE html>
<html lang="en">
	<head>
		  <title><?=isset($title)?$title:'PHC-HMS' ?></title>
		  <!-- Tell the browser to be responsive to screen width -->
		  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		  <!-- Bootstrap 3.3.6 -->
		  <link rel="stylesheet" href="<?= base_url() ?>public/bootstrap/css/bootstrap.min.css">
		  <!-- Font Awesome -->
		  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
		  <!-- Ionicons -->
		  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
		  <!-- Theme style -->
	      <link rel="stylesheet" href="<?= base_url() ?>public/dist/css/AdminLTE.min.css">
          <!--data tables-->
            <link rel="stylesheet" href="<?= base_url() ?>public/plugins/datatables/dataTables.bootstrap.css">
	       <!-- Custom CSS -->
		  <link rel="stylesheet" href="<?= base_url() ?>public/dist/css/style.css">
		  <!-- AdminLTE Skins. Choose a skin from the css/skins. -->
		  <link rel="stylesheet" href="<?= base_url() ?>public/dist/css/skins/skin-blue.min.css">
		  <!-- jQuery 2.2.3 -->
		  <script src="<?= base_url() ?>public/plugins/jQuery/jquery-2.2.3.min.js"></script>
		  <!-- jQuery UI 1.11.4 -->
		  <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
		
	</head>
	<body class="hold-transition skin-blue sidebar-mini">
		<div class="wrapper" style="height: auto;">
			 <?php if($this->session->flashdata('msg') != ''): ?>
			    <div class="alert alert-warning flash-msg alert-dismissible">
			      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			      <?= $this->session->flashdata('msg'); ?> 
			    </div>
			  <?php endif; ?> 
			
			<section id="container">
				<!--header start-->
				<header class="header white-bg">
					<?php include('include/navbar.php'); ?>
				</header>
				<!--header end-->
				<!--sidebar start-->
				<aside>
					<?php include('include/sidebar.php'); ?>
				</aside>
				<!--sidebar end-->
				<!--main content start-->
				<section id="main-content">
					<div class="content-wrapper" style="padding:15px;">
						<!-- page start-->
						<?php $this->load->view($view);?>
						<!-- page end-->
					</div>
				</section>
				<!--main content end-->
				<!--footer start-->
				<footer class="main-footer">
					<strong>Copyright © 2017 <a href="#">Yakairol Engineering Pvt. Ltd.</a></strong> All rights
					reserved.
				</footer>
				<!--footer end-->
			</section>

			<!-- /.control-sidebar -->
			<?php include('include/control_sidebar.php'); ?>

	</div>	
    
	
	<!-- Bootstrap 3.3.6 -->
	<script src="<?= base_url() ?>public/bootstrap/js/bootstrap.min.js"></script>
	<!-- AdminLTE App -->
	<script src="<?= base_url() ?>public/dist/js/app.min.js"></script>
	<!-- AdminLTE for demo purposes -->
	<script src="<?= base_url() ?>public/dist/js/demo.js"></script>
	<!-- page script -->
<!--        data-tables-->


        <script src="<?= base_url() ?>public/plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="<?= base_url() ?>public/plugins/datatables/dataTables.bootstrap.min.js"></script>
        <script src=" https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js" type="text/javascript" language="javascript"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js" type="text/javascript" language="javascript"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" type="text/javascript" language="javascript"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js" type="text/javascript" language="javascript"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js" type="text/javascript" language="javascript"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js" type="text/javascript" language="javascript"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js" type="text/javascript" language="javascript"></script>

        <script type="text/javascript">
	  $(".flash-msg").fadeTo(2000, 500).slideUp(500, function(){
	    $(".flash-msg").slideUp(500);
	});
	</script>


        <style>
            .dataTables_wrapper .dt-buttons {
                float:right;
                padding-left: 12px;
            }
            @media print {
                table td:last-child {display:none}
                table th:last-child {display:none}
            }
            tfoot {
                display: table-header-group;
            }
        </style>


        <script>
            var lines = 1;

            function getKeyNum(e) {
                var keynum;
                // IE
                if (window.event) {
                    keynum = e.keyCode;
                    // Netscape/Firefox/Opera
                } else if (e.which) {
                    keynum = e.which;
                }

                return keynum;
            }

            var limitLines = function (e) {
                var keynum = getKeyNum(e);

                if (keynum === 13) {
                    if (lines >= this.rows) {
                        e.stopPropagation();
                        e.preventDefault();
                    } else {
                        lines++;
                    }
                }
            };

            var setNumberOfLines = function (e) {
                lines = getNumberOfLines(this.value);
            };

            var limitPaste = function (e) {
                var clipboardData, pastedData;

                // Stop data actually being pasted into div
                e.stopPropagation();
                e.preventDefault();

                // Get pasted data via clipboard API
                clipboardData = e.clipboardData || window.clipboardData;
                pastedData = clipboardData.getData('Text');

                var pastedLines = getNumberOfLines(pastedData);

                // Do whatever with pasteddata
                if (pastedLines <= this.rows) {
                    lines = pastedLines;
                    this.value = pastedData;
                }
                else if (pastedLines > this.rows) {
                    // alert("Too many lines pasted ");
                    this.value = pastedData
                        .split(/\r\n|\r|\n/)
                        .slice(0, this.rows)
                        .join("\n ");
                }
            };

            function getNumberOfLines(str) {
                if (str) {
                    return str.split(/\r\n|\r|\n/).length;
                }

                return 1;
            }

            var limitedElements = document.getElementsByClassName('limit-me');

            Array.from(limitedElements).forEach(function (element) {
                element.addEventListener('keydown', limitLines);
                element.addEventListener('keyup', setNumberOfLines);
                element.addEventListener('cut', setNumberOfLines);
                element.addEventListener('paste', limitPaste);
            });
        </script>

        <script>
            $('.numeric').keyup(function () {
                if (!this.value.match(/[0-9]/)) {
                    this.value = this.value.replace(/[^0-9]/g, '');
                }
            });
        </script>
	
	</body>
</html>