
<form id="bededit_form">
    <div id="bedModal" class="modal fade" role="dialog">

        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header bg-green">
                    <h4 class="modal-title"><i class="fa fa-edit"></i>Edit Bed Details</h4>
                    <button type="button" class="close" data-dismiss="modal" style="margin-top: -21px;">&times;</button>

                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="editBedcat">Bed Category</label>
                        <select class="form-control" name="editBedcat" id="editBedcat">
                            <?php foreach($bedcat as $raw)
                            {
                                echo '<option value="'.$raw->category.'">'.$raw->category.'</option>';
                            }
                            ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="editBednum">Bed Number</label>
                        <textarea class="form-control" name="editBednum" id="editBednum"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="editBeddes">Description</label>
                        <textarea class="form-control" name="editBeddes" id="editBeddes"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="hidden" name="customer_id" id="customer_id">
                    <input type="submit" class="btn btn-success" name="action" value="Save" id="action" >
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>
</form>
<form id="bedadd_form">
    <div id="myModal" class="modal fade" role="dialog">

        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header bg-green">
                    <h4 class="modal-title"><i class="fa fa-edit"></i>Add Bed Details</h4>
                    <button type="button" class="close" data-dismiss="modal" style="margin-top: -21px;">&times;</button>

                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="chooseBedcat">Bed Category</label>
                        <select class="form-control" name="chooseBedcat" id="chooseBedcat">
                            <?php foreach($bedcat as $raw)
                            {
                                echo '<option value="'.$raw->category.'">'.$raw->category.'</option>';
                            }
                            ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="addBednum">Bed Number</label>
                        <textarea class="form-control" name="addBednum" id="addBednum"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="addBeddes">Bed Description</label>
                        <textarea class="form-control" name="addBeddes" id="addBeddes"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="submit" class="btn btn-success" name="action" value="ADD" id="action" >
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>
</form>

<section class="content">

    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header" style="margin-bottom: 20px">
                    <h3 class="box-title"><i class="fa fa-bed"></i>Bed Details</h3><a style=" float: right" href="" class="btn bg-green btn-sm " data-toggle="modal" data-target="#myModal"><i class="fa fa-plus"></i>Add New</a>

                </div>
                <!-- /.box-header -->
                <div class="box-body" style="padding-top:0px">
                    <table id="qtyTable" class="table table-bordered table-hover" style="word-break: break-all; table-layout: fixed">
                        <thead>
                        <tr>
                            <th>Bed Id</th>
                            <th>Description</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>

                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>

</section>

<style>
    .dataTables_wrapper .dt-buttons {
        float:right;
        padding-left: 12px;
    }
    @media print {
        table td:last-child {display:none}
        table th:last-child {display:none}
    }
    tfoot {
        display: table-header-group;
    }
</style>
<script>
    $(document).ready(function(){
        var dataTable = $('#qtyTable').DataTable({

            "ordering": false,
            "paging": true,
            "info": false,
            "searching": false,
            "language": {
                searchPlaceholder: "By Username"
            },
            "processing": true,
            "serverSide": false,
            "order": [],
            dom: 'lBfrtip',
            buttons: [
                {
                    name: 'print',
                    extend: "print",
                    className: 'btn btn-success fa fa-print',
                    exportOptions: {
                        columns: [0,1,2,3]
                    },

                },
                {
                    name: 'excelHtml5',
                    extend:'excelHtml5',
                    className: 'btn btn-primary fa fa-file-excel-o ',
                    exportOptions: {
                        columns: [0, 1,2,3]
                    }
                },
                {
                    extend: 'pdfHtml5',
                    title:'',
                    className: 'btn btn-danger fa fa-file-pdf-o ',
                    exportOptions: {
                        modifier: {
                            page: 'current'
                        },
                        columns: [0, 1,2 ,3]
                    },
                }

            ],
            "ajax": {
                url: "<?php echo base_url() . 'admin/Add_bed/bed_details';?>",
                type: "POST"
            },
            "columnDefs": [
                {
                    "orderable": false,

                }
            ]

        });

        $(document).on('submit','#bedadd_form', function (event) {
            event.preventDefault();
            var chooseBedcatlist= $('#chooseBedcat').val();
            var addBednumlist= $('#addBednum').val();
            var addBeddeslist= $('#addBeddes').val();

            if(chooseBedcatlist !='' && addBednumlist!='' && addBeddeslist!='' )
            {
                $.ajax({
                    url:"<?php echo base_url(). 'admin/Add_bed/addBed_action';?>",
                    method: 'POST',
                    data:new FormData(this),
                    contentType:false,
                    processData:false,
                    success:function (data) {
                        alert(data);
                        $('#bedadd_form')[0].reset();
                        $('#myModal').modal('hide');
                        dataTable.ajax.reload();
                    }
                });
            }
            else
            {
                alert("fields required");
            }
        });



        $(document).on('submit','#bededit_form', function (event) {
            event.preventDefault();
            var editBedcatlist= $('#editBedcat').val();
            var editBednumlist= $('#editBednum').val();
            var editBeddeslist= $('#editBeddes').val();

            if(editBedcatlist !='' && editBednumlist!='' && editBeddeslist!='' )
            {
                $.ajax({
                    url:"<?php echo base_url(). 'admin/Add_bed/bed_action';?>",
                    method: 'POST',
                    data:new FormData(this),
                    contentType:false,
                    processData:false,
                    success:function (data) {
                        alert(data);
                        $('#bededit_form')[0].reset();
                        $('#bedModal').modal('hide');
                        dataTable.ajax.reload();
                    }
                });
            }
            else
            {
                alert("fields required");
            }
        });

        $(document).on('click', '.deleteUser', function(){
            var customer_id = $(this).attr("id");
            console.log(customer_id);
            if(confirm("Are you sure you want to delete this?"))
            {
                $.ajax({
                    url:"<?php echo base_url(); ?>admin/Add_bed/delete_bed_detail",
                    method:"POST",
                    data:{customer_id:customer_id},
                    success:function(data)
                    {
                        alert(data);
                        dataTable.ajax.reload();
                    }
                });
            }
            else
            {
                return false;
            }
        });

    });

    $(document).on('click','.updateUser',function(){
        var customer_id = $(this).attr("id");
        $.ajax({
            url:"<?php echo base_url(). 'admin/Add_bed/fetch_bed_detail';?>",
            method:"POST",
            data:{customer_id:customer_id},
            dataType:"json",
            success: function (data) {
                $('#bedModal').modal('show');
                $('#editBedcat').val(data.editBedcat);
                $('#editBednum').val(data.editBednum);
                $('#editBeddes').val(data.editBeddes);
                $('#customer_id').val(customer_id);
                $("#action").val("save");
            }
        })
    });
</script>
<script>
    $("#tables").addClass('active');
    $("#data-tables").addClass('active');
</script>
