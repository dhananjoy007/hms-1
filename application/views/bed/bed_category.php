
<form id="catedit_form">
    <div id="myvendorModal" class="modal fade" role="dialog">

        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header bg-green">
                    <h4 class="modal-title"><i class="fa fa-edit"></i>Edit Bed Category</h4>
                    <button type="button" class="close" data-dismiss="modal" style="margin-top: -21px;">&times;</button>

                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="catName">Category Name</label>
                        <input class="form-control" name="catName" id="catName" type="text">
                    </div>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="catDes">Description</label>
                        <textarea class="form-control" name="catDes" id="catDes"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="hidden" name="customer_id" id="customer_id">
                    <input type="submit" class="btn btn-success" name="action" value="Save" id="action" >
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>
</form>
<form id="catadd_form">
    <div id="myModal" class="modal fade" role="dialog">

        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header bg-green">
                    <h4 class="modal-title"><i class="fa fa-edit"></i>Add Bed Category</h4>
                    <button type="button" class="close" data-dismiss="modal" style="margin-top: -21px;">&times;</button>

                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="addcatName">Category Name</label>
                        <input class="form-control" name="addcatName" id="addcatName" type="text">
                    </div>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="addcatDes">Description</label>
                        <textarea class="form-control" name="addcatDes" id="addcatDes"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="submit" class="btn btn-success" name="action" value="ADD" id="action" >
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>
</form>

<section class="content">

    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header" style="margin-bottom: 20px">
                    <h3 class="box-title"><i class="fa fa-bed"></i>Bed Category Details</h3><a style=" float: right" href="" class="btn bg-green btn-sm " data-toggle="modal" data-target="#myModal"><i class="fa fa-plus"></i>Add New</a>

                </div>
                <!-- /.box-header -->
                <div class="box-body" style="padding-top:0px">
                    <table id="bedcateTable" class="table table-bordered table-hover" style="word-break: break-all; table-layout: fixed">
                        <thead>
                        <tr>
                            <th>Bed Category</th>
                            <th>Description</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>

                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>

</section>

<style>
    .dataTables_wrapper .dt-buttons {
        float:right;
        padding-left: 12px;
    }
    @media print {
        table td:last-child {display:none}
        table th:last-child {display:none}
    }
    tfoot {
        display: table-header-group;
    }
</style>
<script>
    $(document).ready(function(){
        var dataTable = $('#bedcateTable').DataTable({
            "ordering": false,
            "paging":   true,
            "info":    false ,
            "searching":true,
            "language": {
                searchPlaceholder: "By All "
            },
            "processing": true,
            "serverSide": false,
            "order":[],
            "dom": 'lBfrtip',
            buttons: [
                {
                    name: 'print',
                    extend: "print",
                    className: 'btn btn-success fa fa-print',
                    exportOptions: {
                        columns: [0,1]
                    },

                },
                {
                    name: 'excelHtml5',
                    extend:'excelHtml5',
                    className: 'btn btn-primary fa fa-file-excel-o ',
                    exportOptions: {
                        columns: [0, 1,]
                    }
                },
                {
                    extend: 'pdfHtml5',
                    title:'',
                    className: 'btn btn-danger fa fa-file-pdf-o ',
                    exportOptions: {
                        modifier: {
                            page: 'current'
                        },
                        columns: [0, 1, ]
                    },
                }

            ],


            "ajax":{
                url: "<?php echo base_url(). 'admin/Add_Bedcategory/bedcategory_details';?>",
                type: "POST"
            },
            "columnDefs":[
                {
                    "orderable":false,

                }
            ]

        });

        $(document).on('submit','#catadd_form', function (event) {
            event.preventDefault();
            var addcatNamelist= $('#ddcatName').val();
            var addcatDeslist= $('#addcatDes').val();

            if(addcatNamelist !='' && addcatDeslist!='' )
            {
                $.ajax({
                    url:"<?php echo base_url(). 'admin/Add_Bedcategory/addBedcat_action';?>",
                    method: 'POST',
                    data:new FormData(this),
                    contentType:false,
                    processData:false,
                    success:function (data) {
                        alert(data);
                        $('#catadd_form')[0].reset();
                        $('#myModal').modal('hide');
                        dataTable.ajax.reload();
                    }
                });
            }
            else
            {
                alert("fields required");
            }
        });



        $(document).on('submit','#catedit_form', function (event) {
            event.preventDefault();
            var catNamelist= $('#catName').val();
            var catDeslist= $('#catDes').val();

            if(catNamelist !='' && catDeslist!='' )
            {
                $.ajax({
                    url:"<?php echo base_url(). 'admin/Add_Bedcategory/Bedcat_action';?>",
                    method: 'POST',
                    data:new FormData(this),
                    contentType:false,
                    processData:false,
                    success:function (data) {
                        alert(data);
                        $('#catedit_form')[0].reset();
                        $('#myvendorModal').modal('hide');
                        dataTable.ajax.reload();
                    }
                });
            }
            else
            {
                alert("fields required");
            }
        });

        $(document).on('click', '.deleteUser', function(){
            var customer_id = $(this).attr("id");
            console.log(customer_id);
            if(confirm("Are you sure you want to delete this?"))
            {
                $.ajax({
                    url:"<?php echo base_url(); ?>admin/Add_Bedcategory/delete_bedcat_detail",
                    method:"POST",
                    data:{customer_id:customer_id},
                    success:function(data)
                    {
                        alert(data);
                        dataTable.ajax.reload();
                    }
                });
            }
            else
            {
                return false;
            }
        });

    });

    $(document).on('click','.updateUser',function(){
        var customer_id = $(this).attr("id");
        $.ajax({
            url:"<?php echo base_url(). 'admin/Add_Bedcategory/fetch_cat_detail';?>",
            method:"POST",
            data:{customer_id:customer_id},
            dataType:"json",
            success: function (data) {
                $('#myvendorModal').modal('show');
                $('#catName').val(data.catName);
                $('#catDes').val(data.catDes);
                $('#customer_id').val(customer_id);
                $("#action").val("save");
            }
        })
    });
</script>
<script>
    $("#tables").addClass('active');
    $("#data-tables").addClass('active');
</script>
