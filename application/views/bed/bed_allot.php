
<form id="allotedit_form">
    <div id="allotModal" class="modal fade" role="dialog">

        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header bg-green">
                    <h4 class="modal-title"><i class="fa fa-edit"></i>Edit Allotment</h4>
                    <button type="button" class="close" data-dismiss="modal" style="margin-top: -21px;">&times;</button>

                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="editBedid">Bed Id</label>
                        <select class="form-control" name="editBedid" id="editBedid">
                            <?php foreach($bedcat as $raw)
                            {
                                echo '<option value="'.$raw->bed_category.'-'.$raw->bed_num.'">'.$raw->bed_category.'-'.$raw->bed_num.'</option>';
                            }
                            ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="editPatname">Patient Name</label>
                        <textarea class="form-control" name="editPatname" id="editPatname"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="editAllottime">Alloted Time</label>
                        <textarea class="form-control" name="editAllottime" id="editAllottime"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="editDistime">Discharge Time</label>
                        <textarea class="form-control" name="editDistime" id="editDistime"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="hidden" name="customer_id" id="customer_id">
                    <input type="submit" class="btn btn-success" name="action" value="Save" id="action" >
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>
</form>
<form id="allotadd_form">
    <div id="myModal" class="modal fade" role="dialog">

        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header bg-green">
                    <h4 class="modal-title"><i class="fa fa-edit"></i>Add Allotment</h4>
                    <button type="button" class="close" data-dismiss="modal" style="margin-top: -21px;">&times;</button>

                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="chooseBedid">Bed Id</label>
                        <select class="form-control" name="chooseBedid" id="chooseBedid">
                            <?php foreach($bedcat as $raw)
                            {
                                echo '<option value="'.$raw->bed_category.'-'.$raw->bed_num.'">'.$raw->bed_category.'-'.$raw->bed_num.'</option>';
                            }
                            ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="addPatname">Patient Name</label>
                        <textarea class="form-control" name="addPatname" id="addPatname"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="addAllottime">Alloted Time</label>
                        <textarea class="form-control" name="addAllottime" id="addAllottime"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="addDistime">Discharge Time</label>
                        <textarea class="form-control" name="addDistime" id="addDistime"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="submit" class="btn btn-success" name="action" value="ADD" id="action" >
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>
</form>

<section class="content">

    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header" style="margin-bottom: 20px">
                    <h3 class="box-title"><i class="fa fa-bed"></i>Bed Allotments</h3><a style=" float: right" href="" class="btn bg-green btn-sm " data-toggle="modal" data-target="#myModal"><i class="fa fa-plus"></i>Add New</a>

                </div>
                <!-- /.box-header -->
                <div class="box-body" style="padding-top:0px">
                    <table id="qtyTable" class="table table-bordered table-hover" style="word-break: break-all; table-layout: fixed">
                        <thead>
                        <tr>
                            <th>Bed Id</th>
                            <th>Patient</th>
                            <th>Alloted Time</th>
                            <th>Discharge Time</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>

                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>

</section>

<style>
    .dataTables_wrapper .dt-buttons {
        float:right;
        padding-left: 12px;
    }
    @media print {
        table td:last-child {display:none}
        table th:last-child {display:none}
    }
    tfoot {
        display: table-header-group;
    }
</style>
<script>
    $(document).ready(function(){
        var dataTable = $('#qtyTable').DataTable({

            "ordering": false,
            "paging": true,
            "info": false,
            "searching": false,
            "language": {
                searchPlaceholder: "By Username"
            },
            "processing": true,
            "serverSide": false,
            "order": [],
            dom: 'lBfrtip',
            buttons: [
                {
                    name: 'print',
                    extend: "print",
                    className: 'btn btn-success fa fa-print',
                    exportOptions: {
                        columns: [0,1,2,3]
                    },

                },
                {
                    name: 'excelHtml5',
                    extend:'excelHtml5',
                    className: 'btn btn-primary fa fa-file-excel-o ',
                    exportOptions: {
                        columns: [0, 1,2,3]
                    }
                },
                {
                    extend: 'pdfHtml5',
                    title:'',
                    className: 'btn btn-danger fa fa-file-pdf-o ',
                    exportOptions: {
                        modifier: {
                            page: 'current'
                        },
                        columns: [0, 1,2 ,3]
                    },
                }

            ],
            "ajax": {
                url: "<?php echo base_url() . 'admin/Bed_allot/allot_details';?>",
                type: "POST"
            },
            "columnDefs": [
                {
                    "orderable": false,

                }
            ]

        });

        $(document).on('submit','#allotadd_form', function (event) {
            event.preventDefault();
            var chooseBedidlist= $('#chooseBedid').val();
            var addPatnamelist= $('#addPatname').val();
            var addAllottimelist= $('#addAllottime').val();
            var addDistimelist= $('#addDistime').val();

            if(chooseBedidlist !='' && addAllottimelist!='' && addPatnamelist!='' && addDistimelist!='' )
            {
                $.ajax({
                    url:"<?php echo base_url(). 'admin/Bed_allot/addAllot_action';?>",
                    method: 'POST',
                    data:new FormData(this),
                    contentType:false,
                    processData:false,
                    success:function (data) {
                        alert(data);
                        $('#allotadd_form')[0].reset();
                        $('#myModal').modal('hide');
                        dataTable.ajax.reload();
                    }
                });
            }
            else
            {
                alert("fields required");
            }
        });



        $(document).on('submit','#allotedit_form', function (event) {
            event.preventDefault();
            var editBedidlist= $('#editBedid').val();
            var editPatnamelist= $('#editPatname').val();
            var editAllottimelist= $('#editAllottime').val();
            var editDistimelist= $('#editDistime').val();

            if(editBedidlist !='' && editPatnamelist!='' && editAllottimelist!='' && editDistimelist!='' )
            {
                $.ajax({
                    url:"<?php echo base_url(). 'admin/Bed_allot/allot_action';?>",
                    method: 'POST',
                    data:new FormData(this),
                    contentType:false,
                    processData:false,
                    success:function (data) {
                        alert(data);
                        $('#allotedit_form')[0].reset();
                        $('#allotModal').modal('hide');
                        dataTable.ajax.reload();
                    }
                });
            }
            else
            {
                alert("fields required");
            }
        });

        $(document).on('click', '.deleteUser', function(){
            var customer_id = $(this).attr("id");
            console.log(customer_id);
            if(confirm("Are you sure you want to delete this?"))
            {
                $.ajax({
                    url:"<?php echo base_url(); ?>admin/Bed_allot/delete_allot_detail",
                    method:"POST",
                    data:{customer_id:customer_id},
                    success:function(data)
                    {
                        alert(data);
                        dataTable.ajax.reload();
                    }
                });
            }
            else
            {
                return false;
            }
        });

    });

    $(document).on('click','.updateUser',function(){
        var customer_id = $(this).attr("id");
        $.ajax({
            url:"<?php echo base_url(). 'admin/Bed_allot/fetch_allot_detail';?>",
            method:"POST",
            data:{customer_id:customer_id},
            dataType:"json",
            success: function (data) {
                $('#allotModal').modal('show');
                $('#editBedid').val(data.editBedid);
                $('#editPatname').val(data.editPatname);
                $('#editAllottime').val(data.editAllottime);
                $('#editDistime').val(data.editDistime);
                $('#customer_id').val(customer_id);
                $("#action").val("save");
            }
        })
    });
</script>
<script>
    $("#tables").addClass('active');
    $("#data-tables").addClass('active');
</script>
