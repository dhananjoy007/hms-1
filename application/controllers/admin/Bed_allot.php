<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: Charan
 * Date: 10-11-2018
 * Time: 23:27
 */

class Bed_allot extends MY_Controller
{
    public function __construct()
    {
        parent:: __construct();
        $this->load->model('Addbed_model');
        $this->load->model('Bed_allot_model');
    }

    public function index(){
        $data['bedcat'] = $this-> Addbed_model->selectbed();
        $data['view']='bed/bed_allot';
        $this->load->view('admin/layout',$data);
    }



    function fetch_allot_detail()
    {
        $output = array();
        $data = $this->Bed_allot_model->fetch_allot_detail($_POST["customer_id"]);
        foreach ($data as $row)
        {
            $output['editBedid'] = $row->bed_id;
            $output['editPatname'] = $row->patient_name;
            $output['editAllottime'] = $row->alloted_time;
            $output['editDistime'] = $row->discharge_time;
        }
        echo json_encode($output);
    }

    function addAllot_action()
    {
        $_POST['action'] = "ADD";
        if ($_POST['action'] == "ADD") {

            $add_data = array(
                'bed_id' => $this->input->post('chooseBedid'),
                'patient_name' => $this->input->post('addPatname'),
                'alloted_time' => $this->input->post('addAllottime'),
                'discharge_time' => $this->input->post('addDistime'),
            );
            $this->Bed_allot_model->add_allot($add_data);
            echo 'Category Added Successful';
            redirect("Bed_allot", "refresh");
        }
    }
    function allot_action()
    {
        $_POST['action'] = "save";
        if ($_POST['action'] == "save") {
            $updated_data = array(
                'bed_id' => $this->input->post('editBedid'),
                'patient_name' => $this->input->post('editPatname'),
                'alloted_time' => $this->input->post('editAllottime'),
                'discharge_time' => $this->input->post('editDistime'),
            );
            $this->Bed_allot_model->update_allot($this->input->post("customer_id"), $updated_data);
            echo 'Changed Successful';
            redirect("Bed_allot", "refresh");
        }
    }

    function allot_details(){
        $fetch_bed = $this->Bed_allot_model->selectTable();
        $data = array();
        foreach($fetch_bed as $row)
        {
            $sub_array = array();
            $sub_array[] = "$row->bed_id";
            $sub_array[] = $row->patient_name;
            $sub_array[] = $row->alloted_time;
            $sub_array[] = $row->discharge_time;
            $sub_array[] = ' <td>
                                      <div class="btn-group">
                                    <a href="" class="btn btn-info btn-sm updateUser" id="' . $row->allot_id . '" name="updateUser" data-toggle="modal" data-target="#allotModal"><i class="fa fa-edit"></i></a>
                                    <a href="" class="btn btn-danger btn-sm item_delete deleteUser" id="' . $row->allot_id . '" name="deleteUser"  ><i class="fa fa-trash"></i></a>
                                    </div>
                                </td>';
            $data[] = $sub_array;
        }
        $output = array(
            "data"              => $data
        );
        echo json_encode($output);

    }

    function delete_bed_detail()
    {
        $this->Bed_allot_model->delete_allot($_POST["customer_id"]);
        echo 'Record Deleted Successful';
        redirect("Bed_allot", "refresh");
    }


}

?>