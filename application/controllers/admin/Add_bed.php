<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: Charan
 * Date: 10-11-2018
 * Time: 23:27
 */

class Add_bed extends MY_Controller
{
    public function __construct()
    {
        parent:: __construct();
        $this->load->model('Addbed_model');
        $this->load->model('Addbedcat_model');
    }

    public function index(){
        $data['bedcat'] = $this-> Addbedcat_model->selectvendor();
        $data['view']='bed/bed_list';
        $this->load->view('admin/layout',$data);
    }



    function fetch_bed_detail()
    {
        $output = array();
        $data = $this->Addbed_model->fetch_bed_detail($_POST["customer_id"]);
        foreach ($data as $row)
        {
            $output['editBedcat'] = $row->bed_category;
            $output['editBednum'] = $row->bed_num;
            $output['editBeddes'] = $row->bed_des;
        }
        echo json_encode($output);
    }

    function addBed_action()
    {
        $_POST['action'] = "ADD";
        if ($_POST['action'] == "ADD") {
            $status = "Available";
            $add_data = array(
                'bed_category' => $this->input->post('chooseBedcat'),
                'bed_des' => $this->input->post('addBeddes'),
                'bed_num' => $this->input->post('addBednum'),
                'bed_status' => $status,
            );
            $this->Addbed_model->add_bed($add_data);
            echo 'Category Added Successful';
            redirect("Add_bed", "refresh");
        }
    }
    function bed_action()
    {
        $_POST['action'] = "save";
        if ($_POST['action'] == "save") {
            $updated_data = array(
                'bed_category' => $this->input->post('editBedcat'),
                'bed_des' => $this->input->post('editBeddes'),
                'bed_num' => $this->input->post('editBednum'),
            );
            $this->Addbed_model->update_bed($this->input->post("customer_id"), $updated_data);
            echo 'Changed Successful';
            redirect("Add_bed", "refresh");
        }
    }

    function bed_details(){
        $fetch_bed = $this->Addbed_model->selectbed();
        $data = array();
        foreach($fetch_bed as $row)
        {
            $sub_array = array();
            $sub_array[] = "$row->bed_category-$row->bed_num";
            $sub_array[] = $row->bed_des;
            $sub_array[] = '<a href="" class="btn btn-success btn-sm" >'. $row->bed_status .'</a>';
;
            $sub_array[] = ' <td>
                                      <div class="btn-group">
                                    <a href="" class="btn btn-info btn-sm updateUser" id="' . $row->bed_id . '" name="updateUser" data-toggle="modal" data-target="#bedModal"><i class="fa fa-edit"></i></a>
                                    <a href="" class="btn btn-danger btn-sm item_delete deleteUser" id="' . $row->bed_id . '" name="deleteUser"  ><i class="fa fa-trash"></i></a>
                                    </div>
                                </td>';
            $data[] = $sub_array;
        }
        $output = array(
            "data"              => $data
        );
        echo json_encode($output);

    }

    function delete_bed_detail()
    {
        $this->Addbed_model->delete_bed($_POST["customer_id"]);
        echo 'Record Deleted Successful';
        redirect("Add_bed", "refresh");
    }


}

?>