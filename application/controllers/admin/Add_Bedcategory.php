<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: Charan
 * Date: 10-11-2018
 * Time: 23:27
 */

class Add_Bedcategory extends MY_Controller
{
    public function __construct()
    {
        parent:: __construct();
        $this->load->model('Addbedcat_model');
    }

    public function index(){
        $data['view']='bed/bed_category';
        $this->load->view('admin/layout',$data);
    }



    function fetch_cat_detail()
    {
        $output = array();
        $data = $this->Addbedcat_model->fetch_cat_detail($_POST["customer_id"]);
        foreach ($data as $row)
        {
            $output['catName'] = $row->category;
            $output['catDes'] = $row->description;
        }
        echo json_encode($output);
    }

    function addBedcat_action()
    {
        $_POST['action'] = "ADD";
        if ($_POST['action'] == "ADD") {
            $add_data = array(
                'category' => $this->input->post('addcatName'),
                'description' => $this->input->post('addcatDes'),
            );
            $this->Addbedcat_model->add_bedcat($add_data);
            echo 'Category Added Successful';
            redirect("Add_Bedcategory", "refresh");
        }
    }
    function Bedcat_action()
    {
        $_POST['action'] = "save";
        if ($_POST['action'] == "save") {
            $updated_data = array(
                'category' => $this->input->post('catName'),
                'description' => $this->input->post('catDes'),
            );
            $this->Addbedcat_model->update_bedcat($this->input->post("customer_id"), $updated_data);
            echo 'Changed Successful';
            redirect("Add_Bedcategory", "refresh");
        }
    }

    function bedcategory_details(){
        $fetch_data = $this->Addbedcat_model->selectvendor();
        $data = array();
        foreach($fetch_data as $row)
        {
            $sub_array = array();
            $sub_array[] = $row->category;
            $sub_array[] = $row->description;
            $sub_array[] = ' <td>
                                      <div class="btn-group">
                                    <a href="" class="btn btn-info btn-sm updateUser" id="' . $row->cat_id . '" name="updateUser" data-toggle="modal" data-target="#myvendorModal"><i class="fa fa-edit"></i></a>
                                    <a href="" class="btn btn-danger btn-sm item_delete deleteUser" id="' . $row->cat_id . '" name="deleteUser"  ><i class="fa fa-trash"></i></a>
                                    </div>
                                </td>';
            $data[] = $sub_array;
        }
        $output = array(
            "data"              => $data
        );
        echo json_encode($output);

    }

    function delete_bedcat_detail()
    {
        $this->Addbedcat_model->delete_bedcat($_POST["customer_id"]);
        echo 'Record Deleted Successful';
        redirect("Add_bed", "refresh");
    }


}

?>