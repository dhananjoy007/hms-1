<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class AdminLTE extends CI_Controller {
        public function __construct(){
            parent::__construct();
            if(!$this->session->has_userdata('is_admin_login'))
            {
                redirect('admin/auth/login');
            }
        }
		public function index(){
			redirect(base_url('admin/auth'));
		}


		public function chartjs(){
			$data['view'] = 'admin/adminlte/charts/chartjs';
			$this->load->view('admin/layout', $data);
		}
	}